<?php

declare(strict_types = 1);

namespace Drupal\aegis;

use Drupal\Core\Site\Settings as DrupalSettings;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides aegis settings.
 */
final class AegisSettings {

  /**
   * The Drupal settings.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected DrupalSettings $settings;

  /**
   * Constructor for AegisSettings.
   *
   * @param \Drupal\Core\Site\Settings $settings
   *   The Drupal settings.
   */
  public function __construct(DrupalSettings $settings) {
    $this->settings = $settings;
  }

  /**
   * Get the admin hostname represented as a regular expression.
   *
   * @return string|null
   *   The admin hostname from the site settings.
   */
  public function getAdminHostname(): ?string {
    return $this->settings->get('aegis')['admin']['hostname'] ?? NULL;
  }

  /**
   * Get the admin username.
   *
   * @return string|null
   *   The admin username from the site settings.
   */
  public function getUsername(): ?string {
    return $this->settings->get('aegis')['admin']['username'] ?? NULL;
  }

  /**
   * Get the admin password.
   *
   * @return string|null
   *   The admin password from the site settings.
   */
  public function getPassword(): ?string {
    return $this->settings->get('aegis')['admin']['password'] ?? NULL;
  }

  /**
   * Get the public hostname represented as a regular expression.
   *
   * @return string|null
   *   The public hostname from the site settings.
   */
  public function getPublicHostname(): ?string {
    return $this->settings->get('aegis')['hostname'] ?? NULL;
  }

  /**
   * Get the error code for the protected pages.
   *
   * @return int
   *   The error code for the protected paths, defaults to 404 if not set.
   */
  public function getErrorCode(): int {
    return $this->settings->get('aegis')['error_code'] ?? Response::HTTP_NOT_FOUND;
  }

  /**
   * Get the protected paths.
   *
   * @return array
   *   The list of protected paths represented as a regular expression.
   */
  public function getProtectedPaths(): array {
    return $this->settings->get('aegis')['paths'] ?? [
      '^\/admin',
      '^\/user',
      '^\/node\/\d+\/\w+$',
      '^\/media\/\d+\/\w+$',
      '^\/taxonomy\/term\/\d+\/\w+$',
    ];
  }

}
