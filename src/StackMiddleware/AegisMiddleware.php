<?php

declare(strict_types = 1);

namespace Drupal\aegis\StackMiddleware;

use Drupal\aegis\AegisSettings;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Provides the Aegis middleware.
 */
class AegisMiddleware implements HttpKernelInterface {

  /**
   * The previous middleware.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected HttpKernelInterface $app;

  /**
   * The aegis settings.
   *
   * @var \Drupal\aegis\AegisSettings
   */
  protected AegisSettings $settings;

  /**
   * Constructor for AegisMiddleware.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $app
   *   The previous middleware.
   * @param \Drupal\aegis\AegisSettings $settings
   *   The aegis settings.
   */
  public function __construct(HttpKernelInterface $app, AegisSettings $settings) {
    $this->app = $app;
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(
    Request $request,
    $type = HttpKernelInterface::MASTER_REQUEST,
    $catch = TRUE
  ): Response {
    if ($this->isPublicHostname($request) && $this->isProtectedPath($request)) {
      switch ($this->settings->getErrorCode()) {
        default:
        case Response::HTTP_NOT_FOUND:
          throw new NotFoundHttpException();

        case Response::HTTP_FORBIDDEN:
          throw new AccessDeniedHttpException();
      }
    }

    if (!$this->isAdminHostname($request)) {
      return $this->app->handle($request, $type, $catch);
    }

    if (
      $request->getUser() === $this->settings->getUsername()
      && $request->getPassword() === $this->settings->getPassword()
    ) {
      return $this->app->handle($request, $type, $catch);
    }

    throw new UnauthorizedHttpException(
      'Basic',
      'No authentication credentials provided.'
    );
  }

  /**
   * Determine if the current request is the public hostname.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return bool
   *   Whether the current request is the public hostname.
   */
  public function isPublicHostname(Request $request): bool {
    return (bool) preg_match(
      '/' . $this->settings->getPublicHostname() . '/',
      $request->getHost()
    );
  }

  /**
   * Determine if the current request is the admin hostname.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return bool
   *   Whether the current request is the public hostname.
   */
  public function isAdminHostname(Request $request): bool {
    return (bool) preg_match(
      '/' . $this->settings->getAdminHostname() . '/',
      $request->getHost()
    );
  }

  /**
   * Determine if the current request is a protected path.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return bool
   *   Whether the current request is a protected path.
   */
  public function isProtectedPath(Request $request): bool {
    foreach ($this->settings->getProtectedPaths() as $protected_path) {
      if (preg_match('/' . $protected_path . '/', $request->getPathInfo())) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
