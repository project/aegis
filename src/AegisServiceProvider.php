<?php

declare(strict_types = 1);

namespace Drupal\aegis;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Provides services and parameters.
 */
class AegisServiceProvider extends ServiceProviderBase {

  /**
   * Add the url.site global cache context.
   *
   * @param \Drupal\Core\DependencyInjection\ContainerBuilder $container
   *   The container builder.
   */
  public function alter(ContainerBuilder $container): void {
    $renderer_config = $container->getParameter('renderer.config');
    $renderer_config['required_cache_contexts'][] = 'url.site';

    $container->setParameter('renderer.config', $renderer_config);
  }

}
